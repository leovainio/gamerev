# Pelin arvostelu sovellus

## Yleistä

Käytetty AngularJS:ää, sekä localstoragea. Sovelluksessa pysty lisäämään pelejä, muokkaamaan ja poistamaan niitä, sekä laatimaan arvosteluja peleille.


## Käynistysohjeet

- asenna node.js
- käynnistä CMD ja mene kansioon ja kirjoita (`npm install`).
- aja (`npm start`)
- avaa selaimessa http://localhost:8080/app/index.html

## Rakenne
###Viewsit:  
- List-view: Voit selata pelejä, lisätä pelin, etsiä peliä, sortata aakkosittain/arvosanan mukaan
- Review-view: Näet arvostelut ja voit lisätä arvostelun

###Controller
- GameListCtrl: List-viewille tarkoitettu controlleri joka hallinnoi, pelien tuonnin näytölle, pelin lisäyksen, poiston ja muokkauksen
- GameRevCtrl: Review-viewille tarkoitettu controlleri joka hallinnoi pelin tuonnin näytölle, arvostelujen tuonnin näytölle ja arvostelun lisäyksen

### Backend
- `makeid`: lisää  random idn jota käytetään pelin id:nä ja arvostelun idnä
- `getGrades`: tämä käy jokaisen pelin läpi ja jokaisen arvostelun ja asettaa pelille keskiarvon. Tätä käytetään kun haetaan pelit tietokannasta, kun tallentaa uuden pelin sekä kun uusi arvostelu tallennetaan.
- `save`: Tallentaa uuden pelin jos ei ole idtä. Päivittää pelin jos pelillä on jo idn.
- `getAll`: palauttaa localstoragesta pelit. Jos ei ole pelejä asetetaan alotuspeliksi XCOM peli ja sille 2 arvostelua
- `addGameReviw`: Etsii oikean pelin ja tallentaa arvostelun, sekä tallentaa tietokantaan.
- `getOne`: hakee tietyn pelin. Tätä käytetään review-viewissä.
- `delete`: Hakee id:n mukaan oikean pelin ja poistaa sen arraysta käyttämällä splice ominaisuutta, sekä tallentaa localstorageen.



  


## Huomioita

rakennettu AngularJs phonecat tutoriaalin päälle.
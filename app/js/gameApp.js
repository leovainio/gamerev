'use strict';

var gameRevApp = angular.module('gameRevApp', [
  'ngRoute',
  'gameRevControllers',
  'gameRevServices'
]);
gameRevApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/games', {
        templateUrl: 'partials/gamerev-list.html',
        controller: 'GameListCtrl'
      }).
      when('/games/:gameId', {
        templateUrl: 'partials/gamerev-review.html',
        controller: 'GameRevCtrl'
      }).
      otherwise({
        redirectTo: '/games'
      });
  }]);
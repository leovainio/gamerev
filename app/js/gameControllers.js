'use strict';

/* Controllers */

var gameRevControllers = angular.module('gameRevControllers',[]);

//list view
gameRevControllers.controller('GameListCtrl', ['$scope','Game','GameApi',
	function ($scope, Game, GameApi) {
    //
    //$scope.games = Game.quary();
 		$scope.games = GameApi.getAll();		
  	$scope.orderProp = 'title';
    //alustin oman käsittelijän joka tallentaa ja tuo tiedot localstoragesta
    $scope.GameApi= GameApi;

    $scope.addGame=function(){
      $scope.GameApi.save($scope.newGame);
      $scope.newGame={};
      
    };
    $scope.deleteGame=function(id){
      $scope.GameApi.delete(id)
    };

    $scope.editGame=function(id){
      $scope.newGame=angular.copy($scope.GameApi.getOne(id));
    };
}]);

//yhden pelin viewi
gameRevControllers.controller('GameRevCtrl', ['$scope', '$routeParams','Game','GameApi',
  function($scope, $routeParams, Game,GameApi) {
    $scope.game=GameApi.getOne($routeParams.gameId);
    $scope.GameApi= GameApi;
    //$scope.game = Game.get({gameId:$routeParams.gameId});
    $scope.gameId = $routeParams.gameId;
    $scope.addReview=function(){
      var newReview ={
      title:$scope.newGame.title,
      review:$scope.newGame.review,
      grade:$scope.newGame.grade
      };
     $scope.GameApi.addGameReview(newReview,$scope.gameId);
     //hide modal
     angular.element('#myModal').modal('hide');
    };
  }]);
var gameRevServices = angular.module('gameRevServices', ['ngResource']);


gameRevServices.factory('Game', ['$resource',
  function($resource){
    return $resource('reviews/:gameId.json', {}, {
      query: {method:'GET', params:{gameId:'reviews'}, isArray:true},

    });
  }]);

gameRevServices.service('GameApi',function(){
	var storageID='gameRev1'
	var id =localStorage.length;
	var games=[];

	//tekee random id:n, oli ongelmia seurata id:n kulkua niin tälläisellä sen pystyy hoitaa
	this.makeid=function(){
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < 5; i++ )
		    text += possible.charAt(Math.floor(Math.random() * possible.length));

		 return text;
		}

	this.getGrades=function(games){
		for (i in games){
			var count=0;
			 var tempCount=0;
			 var tempJ=0;
			 var TempCount=0;
			 var j = 0;
			 for(j in games[i].reviews){
			 	count+=Number(games[i].reviews[j].grade);	
			 }
			 var tempJ=Number(j)+1;
			 var tempCount=count/tempJ;
			 games[i].grade=Math.round(tempCount);
		}
	}

	this.save=function(newGame){
	if(null==newGame.id){
		var game={
			title:newGame.title,
			reviews:[],
			grade:"",
			id:this.makeid()
			}
			games.push(game)
		}else{
			for (i in games){
				if(games[i].id==newGame.id){
					games[i]=newGame;
			}
		}
		}
		this.getGrades(games);
		localStorage.setItem(storageID,JSON.stringify(games));
		
	};
	this.getAll=function(){
		game_list=localStorage.getItem(storageID);
		if(game_list!=null){	
			games=JSON.parse(game_list);
		}else{
		//
		games=[{"id":"jez34",
     			"title": "XCOM",
     			"reviews":[
     						{
     						"title":"hieno peli",
     						"review":"Tyylikäs ja mahtipontinen. En välttämättä suosittele kaikille mutta niille jotka tykkäävät vuoropohjaisista peleistä",
     						"grade":4,
     						"id":"234z3"},
     						{
     						"title":"Lattea",
     						"review":"En ymmärrä miksi joku tykkää pelata pelejä joissa pitää odottaa että tietokone siirtää vuoroja, ajan tuhlausta",
     						"grade":1,
     						"id":"j33ps"}
     						],   
     			"grade":""}];
			}
		this.getGrades(games);
		return games;
	};
	this.addGameReview=function(review,id){
		for (i in games){
			if(games[i].id==id){
				review.id=this.makeid();
				games[i].reviews.push(review);
			}
		}
		this.getGrades(games);
		localStorage.setItem(storageID,JSON.stringify(games));

	};
	this.getOne=function(id){
		for (i in games){
			if(games[i].id==id){
				return games[i];
			}
		}
	};
	this.delete=function(id){
		for (i in games){
			if(games[i].id==id){
				games.splice(i, 1);
			}
		}
		localStorage.setItem(storageID,JSON.stringify(games));
	};
});